#!/bin/bash

usage() {
    local exit_code="${1:-1}"
    echo "Usage: $0 -c <config.conf> [<project> [<project> ...]]" 1>&2
    echo "       $0 -h" 1>&2
    exit "$exit_code"
}

fatal() {
    echo "fatal: $*" 1>&2
    exit 1
}

got_config=0

while getopts 'hc:' opt; do
    case "$opt" in
        c)
            got_config=1
            source "$OPTARG"
            ;;
        h)
            usage 0
            ;;
        *)
            usage
            ;;
    esac
done

shift $((OPTIND - 1))

((got_config)) || usage

which jq >/dev/null || fatal 'missing required program `jq`'
which curl >/dev/null || fatal 'missing required program `curl`'
[[ "$GITLAB_TOKEN" ]] || fatal "missing GITLAB_TOKEN"
[[ "$GITLAB_URL" ]] || fatal "missing GITLAB_URL"
[[ "$GITLAB_CREATE_JSON" ]] || fatal "missing GITLAB_CREATE_JSON"
[[ "$GITLAB_DEPLOY_KEY_JSON" ]] || fatal "missing GITLAB_DEPLOY_KEY_JSON"

gitlab_post_json() {
    local api_path="$1"
    shift
    curl --request POST --header "PRIVATE-TOKEN: $GITLAB_TOKEN" \
     --header "Content-Type: application/json" --data "@-" \
     --url "$GITLAB_URL/api/v4/$api_path" "$@"
}

create_projects() {
    local create_json project_id deploy_key_json
    for project in "$@"; do
        create_json="$(
            jq -n --arg project "$project" "$GITLAB_CREATE_JSON" |
            gitlab_post_json "projects/" --fail)" ||
            fatal "creating project failed"
        project_id="$(jq -e ".id" <<<"$create_json")" ||
            fatal "missing project id"
        echo "created project $project with id $project_id"
        deploy_key_json="$(
            jq -n --arg project "$project" "$GITLAB_DEPLOY_KEY_JSON" |
            gitlab_post_json "projects/$project_id/deploy_keys/" --fail)" ||
            fatal "adding deploy key failed"
        echo "added deploy key"
    done
}

create_projects "$@"