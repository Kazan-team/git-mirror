#!/bin/bash
set -e

trap 'status=$?; echo "Test Failed" >&2; exit $status' ERR

git_mirror="$(dirname "$0")/git-mirror"

function assert_identical_files()
{
    if ! cmp -s "$1" "$2" &>/dev/null; then
        echo "files don't match:"
        diff --color=auto -u "$1" "$2"
        (exit 1)
    fi
}

rm -rf test
git init test/repo1
git init --bare test/repo2.git
GIT_DIR=test/repo1/.git git commit --allow-empty -m "commit1"
"$git_mirror" setup test/mirror1 "$(pwd)"/test/repo1 "$(pwd)"/test/repo2.git
GIT_DIR=test/repo1/.git git branch --list -vv > test/repo1-branches
GIT_DIR=test/repo2.git git branch --list -vv > test/repo2-branches
assert_identical_files test/repo1-branches test/repo2-branches

echo checking that post-receive is only called when it should be...
post_receive_hook=test/mirror1/target.git/hooks/post-receive
cp "$post_receive_hook" test/post-receive.bak
cat > "$post_receive_hook" <<EOF
#!/bin/sh
echo "ERROR: post-receive called for a no-op update"
exit 1
EOF
"$git_mirror" update test/mirror1
"$git_mirror" update test/mirror1
echo passed.
GIT_DIR=test/repo1/.git git commit --allow-empty -m "commit2"
{ ! "$git_mirror" update test/mirror1; } |& grep 'ERROR: post-receive called for a no-op update' >&/dev/null || echo "post-receive not called when it should be"
cp test/post-receive.bak "$post_receive_hook"

GIT_DIR=test/repo1/.git git commit --allow-empty -m "commit3"
"$git_mirror" update test/mirror1
GIT_DIR=test/repo1/.git git branch --list -vv > test/repo1-branches
GIT_DIR=test/repo2.git git branch --list -vv > test/repo2-branches
assert_identical_files test/repo1-branches test/repo2-branches

echo "testing that new tags are pushed..."
GIT_DIR=test/repo1/.git git tag tag1
"$git_mirror" update test/mirror1
GIT_DIR=test/repo1/.git git rev-parse --symbolic --all > test/repo1-refs
GIT_DIR=test/repo2.git git rev-parse --symbolic --all > test/repo2-refs
assert_identical_files test/repo1-refs test/repo2-refs

echo "testing that tags are updated..."
GIT_DIR=test/repo1/.git git tag --delete tag1
GIT_DIR=test/repo1/.git git tag tag1 HEAD~1
"$git_mirror" update test/mirror1
GIT_DIR=test/repo1/.git git rev-parse --symbolic --all > test/repo1-refs
GIT_DIR=test/repo2.git git rev-parse --symbolic --all > test/repo2-refs
assert_identical_files test/repo1-refs test/repo2-refs

echo "testing that new branches are pushed..."
GIT_DIR=test/repo1/.git git branch branch2 master
"$git_mirror" update test/mirror1
GIT_DIR=test/repo1/.git git branch --list -vv > test/repo1-branches
GIT_DIR=test/repo2.git git branch --list -vv > test/repo2-branches
assert_identical_files test/repo1-branches test/repo2-branches

echo "testing force pushes"
GIT_DIR=test/repo1/.git git commit --allow-empty --amend -m "commit4"
"$git_mirror" update test/mirror1
GIT_DIR=test/repo1/.git git branch --list -vv > test/repo1-branches
GIT_DIR=test/repo2.git git branch --list -vv > test/repo2-branches
assert_identical_files test/repo1-branches test/repo2-branches

echo "testing that tags are removed..."
GIT_DIR=test/repo1/.git git tag --delete tag1
"$git_mirror" update test/mirror1
GIT_DIR=test/repo1/.git git rev-parse --symbolic --all > test/repo1-refs
GIT_DIR=test/repo2.git git rev-parse --symbolic --all > test/repo2-refs
assert_identical_files test/repo1-refs test/repo2-refs

echo "testing that branches are removed..."
GIT_DIR=test/repo1/.git git branch -D branch2
"$git_mirror" update test/mirror1
GIT_DIR=test/repo1/.git git rev-parse --symbolic --all > test/repo1-refs
GIT_DIR=test/repo2.git git rev-parse --symbolic --all > test/repo2-refs
assert_identical_files test/repo1-refs test/repo2-refs

echo "All tests passed!!"
